//
//  DettaglioMenuItemViewController.swift
//  Order Coffee
//
//  Created by Ivan Martinelli on 06/06/18.
//  Copyright © 2018 Maieutica. All rights reserved.
//

import UIKit

class DettaglioMenuItemViewController: UIViewController {
    var shop = Shop("ini")
    var menuItems : [MenuItem] = []
    var menuItem : MenuItem = MenuItem("")
    @IBOutlet weak var name: UITextField!
    @IBOutlet weak var price: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.shop = ShopManager.shared.getCurrentShop()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    @IBAction func addItem(_ sender: Any) {
        if (self.menuItems != nil){
            if (name.text != "" && price.text != ""){
                var mi = MenuItem(name.text!)
                do {
                    var importo = price.text!.replacingOccurrences(of: ",", with: ".")
                    if let miPrice : Double = Double(importo) {
                        mi.price = miPrice
                        ShopManager.shared.addItemToCurrentShop(item: mi)
                        self.dismiss(animated: true, completion: nil)
                    }else{
                        print("ERRORE DI FORMATTAZIONE DA GESTIRE")
                    }
                } catch {
                    print("DettaglioMenuItemViewController.addItem Failed")
                }
            }
        }
    }
    
    @IBAction func cancelAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }

}
