//
//  MenuItemTableViewCell.swift
//  Order Coffee
//
//  Created by Ivan Martinelli on 06/06/18.
//  Copyright © 2018 Maieutica. All rights reserved.
//

import UIKit

class MenuItemTableViewCell: UITableViewCell {
    
    @IBOutlet weak var menuItem: UILabel!
    @IBOutlet weak var counter: UILabel!
    @IBOutlet weak var price: UILabel!
    var orderView : OrderViewController? = nil
    var mi : MenuItem? = nil
    
    @IBAction func decrease(_ sender: Any) {
        OrderManager.shared.removeItem(item: mi!)
        counter.text = "\(OrderManager.shared.getCount(item: mi!))"
        orderView?.updateTotal()
    }
    
    @IBAction func increase(_ sender: Any) {
        OrderManager.shared.addItem(item: mi!)
        counter.text = "\(OrderManager.shared.getCount(item: mi!))"
        orderView?.updateTotal()
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
