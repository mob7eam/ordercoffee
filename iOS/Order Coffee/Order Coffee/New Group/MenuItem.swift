//
//  MenuItem.swift
//  Order Coffee
//
//  Created by Ivan Martinelli on 06/06/18.
//  Copyright © 2018 Maieutica. All rights reserved.
//

import Foundation

struct MenuItem : Hashable{
    var name : String
    var price : Double
    var counter : Int
    
    init(_ name: String){
        self.name = name
        price = 0.0
        counter = 0
    }
    
    static func == (lhs: MenuItem, rhs: MenuItem) -> Bool {
        return lhs.name == rhs.name
    }
    
    var hashValue: Int {
        return name.hashValue
    }
}

