//
//  MenuItem.swift
//  Order Coffee
//
//  Created by Ivan Martinelli on 06/06/18.
//  Copyright © 2018 Maieutica. All rights reserved.
//

import Foundation

struct Shop {
    var name : String
    var menuItems : [MenuItem]
    
    static func == (lhs: Shop, rhs: Shop) -> Bool {
        return lhs.name == rhs.name
    }
    
    var hashValue: Int {
        return name.hashValue
    }
    
    init(_ name: String){
        self.name = name
        self.menuItems = []
    }
    
    mutating func addMenuItem(menuItem : MenuItem){
        menuItems.append(menuItem)
    }
}
