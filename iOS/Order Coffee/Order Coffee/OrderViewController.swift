//
//  ViewController.swift
//  Order Coffee
//
//  Created by Ivan Martinelli on 04/06/18.
//  Copyright © 2018 Maieutica. All rights reserved.
//

import UIKit

class OrderViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var totaleTesto: UILabel!
    @IBOutlet weak var table: UITableView!
    var shop : Shop = Shop("init")
    var totale : Double = 0.0
    private let refreshControl = UIRefreshControl()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if #available(iOS 10.0, *) {
            table.refreshControl = refreshControl
        } else {
            table.addSubview(refreshControl)
        }
        refreshControl.addTarget(self, action: #selector(azzeraOrdine(_:)), for: .valueChanged)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.shop = ShopManager.shared.getCurrentShop()
        self.table.reloadData()
        self.updateTotal()
    }
    
    @objc private func azzeraOrdine(_ sender: Any) {
        OrderManager.shared.resetOrder()
        self.table.reloadData()
        self.updateTotal()
        self.refreshControl.endRefreshing()
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return shop.menuItems.count
    }
  
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! MenuItemTableViewCell
        cell.orderView = self
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        cell.mi = shop.menuItems[indexPath.row]
        cell.menuItem.text = shop.menuItems[indexPath.row].name
        cell.price.text = "\(OrderManager.euroFormatter(price: shop.menuItems[indexPath.row].price))"
        cell.counter.text = "\(OrderManager.shared.getCount(item: cell.mi!))"
        return cell
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == UITableViewCellEditingStyle.delete {
            shop = ShopManager.shared.deleteItem(item: shop.menuItems[indexPath.row], shop: self.shop)
            tableView.reloadData()
        }
    }
    
    func updateTotal(){
        var total : Double = 0
        for orderPosition in shop.menuItems{
            total += Double(OrderManager.shared.getCount(item: orderPosition)) * orderPosition.price
        }
        self.totale = total
        totaleTesto.text = "\(OrderManager.euroFormatter(price: totale))€"
    }
    
}

