//
//  OrderManager.swift
//  Order Coffee
//
//  Created by Ivan Martinelli on 17/06/18.
//  Copyright © 2018 Maieutica. All rights reserved.
//

import Foundation

class OrderManager {
    static let shared = OrderManager()
    
    private var order : [MenuItem : Int] = [:]
    
    private init(){
        
    }
    
    func getOrder() -> [MenuItem : Int]{
        return order
    }
    
    func getCount(item : MenuItem) -> Int{
        var count = 0
        if let counter = order[item] {
            count = counter
        }
        return count
    }
    
    func addItem(item : MenuItem) -> [MenuItem : Int]{
        if let counter = order[item] {
            order[item] = (counter + 1)
        } else{
            order[item] = 1
        }
        return order
    }
    
    func removeItem(item : MenuItem) -> [MenuItem : Int]{
        if let counter = order[item] {
            if (counter > 0 ){
                order[item] = (counter - 1)
            }
        } else{
            order[item] = 0
        }
        return order
    }
    
    func resetOrder() -> [MenuItem : Int]{
        for item in order {
            order[item.key] = 0
        }
        return order
    }
    
    static func euroFormatter(price : Double) -> String {
        let formatter = NumberFormatter()
        formatter.minimumFractionDigits = 2 // minimum number of fraction digits on right
        formatter.maximumFractionDigits = 2 // maximum number of fraction digits on right, or comment for all available
        formatter.minimumIntegerDigits = 1 // minimum number of integer digits on left (necessary so that 0.5 don't return .500)
        let formattedNumber = formatter.string(from: NSNumber.init(value: price))
        return formattedNumber!.replacingOccurrences(of: ".", with: ",")
    }
}
