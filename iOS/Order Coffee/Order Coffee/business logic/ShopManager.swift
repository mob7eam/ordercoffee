//
//  MenuItemsManager.swift
//  Order Coffee
//
//  Created by Ivan Martinelli on 06/06/18.
//  Copyright © 2018 Maieutica. All rights reserved.
//

import Foundation
import CoreData
import UIKit

class ShopManager {
    static let shared = ShopManager()
    private var shopCurrentName = "SampleShop"
    var context : NSManagedObjectContext
    
    private init(){
        let appDelegate: AppDelegate = UIApplication.shared.delegate as! AppDelegate
        self.context = appDelegate.persistentContainer.viewContext
    }
    
    static func getShop(withName shopname: String) -> Shop{
        var shop = Shop(shopname)
        let appDelegate: AppDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Item")
        request.predicate = NSPredicate(format: "shop = %@", shopname)
        request.returnsObjectsAsFaults = false
        do {
            let result = try context.fetch(request)
            for data in result as! [NSManagedObject] {
                print(data.value(forKey: "name") as! String)
                var mi = MenuItem(data.value(forKey: "name") as! String)
                mi.price = data.value(forKey: "price") as! Double
                shop.addMenuItem(menuItem: mi)
            }
        } catch {
            print("ShopManager.getShop Failed")
        }
        return shop
    }
    
    static func saveShop(shop: Shop){

    }
    
    func setCurrentShop(shopName : String) -> Void{
        self.shopCurrentName = shopName
    }
    
    func getCurrentShop() -> Shop{
        return ShopManager.getShop(withName: self.shopCurrentName)
    }
    
    func addItemToCurrentShop(item: MenuItem) -> Shop{
        let entity = NSEntityDescription.entity(forEntityName: "Item", in: context)
        let newItem = NSManagedObject(entity: entity!, insertInto: context)
        newItem.setValue(item.name, forKey: "name")
        newItem.setValue(item.price, forKey: "price")
        newItem.setValue(shopCurrentName, forKey: "shop")
        do {
            try context.save()
        } catch {
            print("Failed saving - addItemToCurrentShop")
        }
        return getCurrentShop()
    }
    
    func deleteItem(item: MenuItem, shop: Shop) -> Shop{
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Item")
        request.predicate = NSPredicate(format: "name = %@ and shop = %@", item.name, shop.name)
        request.returnsObjectsAsFaults = false
        do {
            let result = try context.fetch(request)
            for data in result as! [NSManagedObject] {
                print("Risultato trovato \(data.value(forKey: "name") as! String)")
                context.delete(data)
                do {
                    try context.save()
                } catch {
                    print("Failed saving - deleteItem")
                }
            }
        } catch {
            print("ShopManager.getShop Failed")
        }
        return ShopManager.getShop(withName: shop.name)
    }
    
}
